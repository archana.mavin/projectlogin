'use strict';

let config = {
  "database": {
    "host": "localhost",
    "user": "root",
    "password": "root",
    "databaseName": "projectlogin"
  },
  "authorization": {
    "jwtSecret": "projectlogin"
  }
};

module.exports = config;