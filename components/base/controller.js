'use strict';

class baseController {
    constructor() {
    }

    static formatResponse(data, error, callback) {
        
        let result = {
            'data': data
        };

        if (error != null) {
            result.status = false;
            result.hasErrors = true;
            result.errors = [error];
        } else {
            result.status = true;
            result.hasErrors = false;
            result.errors = [];
        }
        callback(result);
    }
}

module.exports = baseController;