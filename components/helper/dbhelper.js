var mysql = require('mysql');
const config = require('../../config/config.js');

var connection = mysql.createConnection({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.databaseName
});

connection.connect((error) => {
    if (error) {
        console.log(error);
    } else {
       console.log('Connected');
   }
});

module.exports = connection;