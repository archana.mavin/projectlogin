const express = require('express');
const bodyParser = require("body-parser");
const config = require('../../config/config');

const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({
    extended: false
});

module.exports = function (app) {

    const userController = require('./userController');

    app.post('/user/register', jsonParser, (req, res) => {
        userController.register(req.body, (data, error) => {
            res.send(data || error);
            res.end();
        })
    });

    app.post('/user/login', jsonParser, (req, res) => {
        userController.login(req.body, (data, error) => {
            console.log(data);
            res.send(data || error);
            res.end();
        })
    });

}


