var config = require('../../config/config');
var db = require('./../helper/dbhelper');
const baseController = require('../base/controller');
const userModel = require('./userModel');

var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

class userController extends baseController {

    static register(body, callback) {
        var userdetails = {
            "register_type": body.type,
            "username": body.name,
            "email": body.email,
            "password": Buffer.from(body.password).toString('base64'),
            "facebookid": body.facebookid,
            "token": body.token,
            "googleid": body.googleid,
            "created": new Date()
        }
        if (body.email != null && re.test(body.email) == true) {
            userModel.checkEmail(body.email, (data, error) => {
                if (error) {
                    callback(null, error);
                } else if (data[0].count != 0) {
                    callback(null, 'Email Id already exist');
                }
                userModel.registerUser(userdetails, (data, error) => {
                    super.formatResponse(data, error, result => {
                        callback(result);
                    });
                });
            });
        }
    }

    static login(body, callback) {
        var logindetail = {
            "register_type": body.type,
            "email": body.email,
            "password": Buffer.from(body.password).toString('base64'),
            "facebookid": body.facebookid,
            "googleid": body.googleid,
        }
        userModel.loginUser(logindetail, (data, error) => {
            super.formatResponse(data, error, result => {
                callback(result);
            });
        });
    }
}

// var today = new Date();
// var type = body.type;
// var email = body.email;
// var users = {
//     "register_type": body.type,
//     "username": body.name,
//     "email": body.email,
//     "password": body.password,
//     "facebookid": body.facebookid,
//     "token": body.token,
//     "googleid": body.googleid,
//     "created": today
// }
// if (email != null && re.test(email) == true) {
//     db.query('SELECT count(id) as count FROM users WHERE email=?', [email], function (error, result) {
//         if (error) {
//             res.json({ status: false, error: error })
//         } else if (result[0].count == 0) {
//             if (type === 1) { // Register by email
//                 db.query('INSERT INTO users SET ?', users, (error, results) => {
//                     if (error) {
//                         res.json({
//                             status: false,
//                             error: error
//                         })
//                     } else {
//                         res.json({
//                             status: true,
//                             data: results,
//                             message: 'user registered sucessfully'
//                         })
//                         res.end();
//                     }
//                 });
//             }

//             if (type === 2) { // Register by facebook
//                 db.query('INSERT INTO users SET ?', users, (error, results) => {
//                     if (error) {
//                         res.json({
//                             status: false,
//                             error: error
//                         })
//                     } else {
//                         res.json({
//                             status: true,
//                             data: results,
//                             message: 'user registered sucessfully'
//                         })
//                         res.end();
//                     }
//                 });
//             }

//             if (type === 3) { // Register by googleid
//                 db.query('INSERT INTO users SET ?', users, (error, results) => {
//                     if (error) {
//                         res.json({
//                             status: false,
//                             error: error
//                         })
//                     } else {
//                         res.json({
//                             status: true,
//                             data: results,
//                             message: 'user registered sucessfully'
//                         })
//                         res.end();
//                     }
//                 });
//             }
//         } else {
//             res.json({ status: false, message: 'Email Id already exist' })
//         }
//     })
// } else {
//     return { status: false, message: "Invalid Email" };
// }
// }

// static login(req, res) {
//     var type = req.body.type;
//     if (type == 1) {
//         var email = req.body.email;
//         var password = req.body.password;
//         db.query('select * from users where email=?', [email], function (err, result) {
//             if (error) {
//                 res.json({ status: false, error: err });
//             }
//             else if (result[0].password == password) {
//                 res.json({ status: true, data: result, message: 'logged in sucessfully' });
//             }
//             else {
//                 res.json({ status: false, message: 'incorrect Email or password' });
//                 res.end();
//             }
//         });
//     }

//     if (type == 2) {
//         var facebookid = req.body.facebookid;
//         db.query('select * from users where facebookid=?', [facebookid], function (err, result) {
//             if (error) {
//                 res.json({ status: false, error: err });
//             }
//             else if (result.length == 0) {
//                 res.json({ status: false, message: 'Error during login' });
//             } else {
//                 res.json({ status: true, data: result, message: 'login failed' });
//                 res.end();
//             }
//         })
//     }

//     if (type == 3) {
//         var googleid = req.body.googleid;
//         db.query('select * from users where googleid=?', [googleid], function (err, result) {
//             if (error) {
//                 res.json({ status: false, error: err });
//             }
//             else if (result.length == 0) {
//                 res.json({ status: false, message: 'Error during login' });
//             } else {
//                 res.json({ status: true, data: result, message: 'login failed' });
//                 res.end();
//             }
//         })
//     }
// }
//}
module.exports = userController;