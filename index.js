var http = require("http");
const express = require('express');
const app = express();
var bodyParser = require('body-parser');

const userApi = require('./components/user/userApi.js');
const userApp = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api', userApp);
userApi(userApp);

const port = 8081;
app.set('port', port);
const server = http.createServer(app);

server.listen(port);
console.log('RESTful API server started on: ' + port);

module.exports=server;

